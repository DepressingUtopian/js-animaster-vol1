(function main() {
    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            animaster().fadeIn(block, 5000);
            // animaster().resetFadeIn(block);            
        });

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            animaster().move(block, 1000, { x: 100, y: 10 });
            // animaster().resetMove(block, 1000, {x: 100, y: 10});                        
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            animaster().scale(block, 1000, 1.25);
            // animaster().resetScale(block);            
        });

    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            animaster().fadeOut(block, 5000);
            // animaster().resetFadeOut(block);            
        });

    let moveAndHideStopper;
    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            moveAndHideStopper = animaster().moveAndHide(block, 5000);
        });


    document.getElementById('moveAndHideReset')
        .addEventListener('click', function () {
            moveAndHideStopper.stop();
        })


    document.getElementById('showAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('showAndHideBlock');
            animaster().showAndHide(block, 5000);
        });

    let heartBeatingTimerId;
    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            heartBeatingTimerId = animaster().heartBeating(block);
        });

    document.getElementById('heartBeatingStop')
        .addEventListener('click', function () {
            if (heartBeatingTimerId !== undefined) {
                heartBeatingTimerId.stop();
                heartBeatingTimerId = undefined;
            }
        });

    let shakingTimerId;
    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            shakingTimerId = animaster().shaking(block);
        });

    document.getElementById('shakingStop')
        .addEventListener('click', function () {
            if (shakingTimerId !== undefined) {
                shakingTimerId.stop();
                shakingTimerId = undefined;
            }
        });
})();


function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(' ');
}

function animaster() {

    /**
     * Функция имитирующая сердцебиение (одну итерацию)
     * @param {HTMLElement} — HTMLElement, с которым нужно взаимодействовать
     */
    function heartBeatingStep(element) {
        animaster().scale(element, 500, 1.4);
        setTimeout(() => { animaster().scale(element, 500, 1) }, 500);
    }

    /**
     * Функция встряхивающая элемент (одна итерацию)
     * @param {HTMLElement}  — HTMLElement, который надо встрясти
     */
    function shakingStep(element) {
        animaster().move(element, 250, { x: 20, y: 0 });
        setTimeout(() => { animaster().move(element, 250, { x: 0, y: 0 }) }, 250);
    }

    let animasterObject = {
        /**
         * Функция заставляет элемент плавно появляться
         * @param element  — HTMLElement, с которым нужно взаимодействовать
         * @param duration — длитеьльность анимации в милисекундах
         */
        fadeIn: function (element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('hide');
            element.classList.add('show');
        },
        /**
         * Функция, сбрасывает анимацию FadeIn
         * @param element — HTMLElement, который надо анимировать
         */
        resetFadeIn: function (element) {
            element.classList.remove('show');
            element.classList.add('hide');
            element.style.transitionDuration = null;
        },
        /**
         * Функция заставляет элемент плавно исчезать 
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param translation — Объект с полями x и y, обозначающими смещение блока
         */
        fadeOut: function (element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove('show');
            element.classList.add('hide');
        },
        /**
         * Функция, сбрасывает анимацию fadeOut
         * @param element — HTMLElement, который надо анимировать
         */
        resetFadeOut: function (element) {
            element.style.transitionDuration = null;
            element.classList.remove('hide');
            element.classList.add('add');
        },
        /**
         * Функция, передвигающая элемент
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param translation — объект с полями x и y, обозначающими смещение блока
         */
        move: function (element, duration, translation) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(translation, null);
        },
        /**
         * Функция, увеличивающая/уменьшающая элемент
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
         */
        scale: function (element, duration, ratio) {
            element.style.transitionDuration = `${duration}ms`;
            element.style.transform = getTransform(null, ratio);
        },
        /**
         * Функция сбрасывает изменение положения и размера
         * @param element — HTMLElement, который надо анимировать      
         */
        resetMoveAndScale: function (element) {
            element.style.transitionDuration = null;
            element.style.transform = null;
        },
        /**
         * Функция сначала сдвигает элемент потом плавно прячет
         * @param element — HTMLElement, который надо анимировать
         * @param duration — общая длительность анимации         
         */
        moveAndHide: function (element, duration) {
            animaster().move(element, duration * 2 / 5, { x: 100, y: 20 });
            let moveAndHideTimerId = setTimeout(() => {
                animaster().fadeOut(element, duration * 3 / 5)
            }, duration * 2 / 5);
            return {
                stop: () => {
                    clearTimeout(moveAndHideTimerId);
                    animaster().resetFadeOut(element);
                    animaster().resetMoveAndScale(element);
                }
            };
        },
        /**
         * Функция сначала показывает элемент, затем плавно размывает
         * @param element — HTMLElement, который надо анимировать
         * @param duration — общая длительность анимации         
         */
        showAndHide: function (element, duration) {
            animaster().fadeIn(element, duration * 1 / 3);
            setTimeout(() => { animaster().fadeOut(element, duration * 1 / 3) }, duration * 2 / 3);
        },
        /**
         * Функция бесконечно  имитирующая сердцебиение
         * @param {HTMLElement} — HTMLElement, с которым нужно взаимодействовать
         */
        heartBeating: function (element) {
            heartBeatingStep(element);
            let timerId = setInterval(() => {
                heartBeatingStep(element);
            }, 1000);
            return {
                stop: () => { clearInterval(timerId); }
            }
        },
        /**
         * Функция бесконечно встряхивающая элемент
         * @param {HTMLElement} — HTMLElement, который надо всрести 
         */
        shaking: function (element) {
            shakingStep(element);
            let timerId = setInterval(() => {
                shakingStep(element);
            }, 500);
            return {
                stop: () => { clearInterval(timerId); }
            };
        },
    }

    return animasterObject;
}
